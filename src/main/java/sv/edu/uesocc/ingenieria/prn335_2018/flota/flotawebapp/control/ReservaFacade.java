package sv.edu.uesocc.ingenieria.prn335_2018.flota.flotawebapp.control;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

@Stateless
@LocalBean
public class ReservaFacade {

    @PersistenceContext(unitName = "sv.edu.uesocc.ingenieria.prn335_2018.flota.datos_FlotaWebApp_war_1.0-SNAPSHOTPU", type = PersistenceContextType.EXTENDED)
    public EntityManager em;

    public int countByIdTipoVehiculo(final Integer idTipoVehiculo) throws java.lang.IllegalStateException, java.lang.IllegalArgumentException {
        int resultado = 0;
        try {
            resultado = em.createNamedQuery("SELECT a FROM EstadoReserva a WHERE a.idReserva.idVehiculo.idModelo.idTipoVehiculo.idTipoVehiculo=" + idTipoVehiculo.toString()).getResultList().size();
        } catch (Exception e) {
            System.out.println("Error" + e);
        }
//        em.close();
        return resultado;
    }
}
